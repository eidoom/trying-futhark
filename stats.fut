def mean [n] (vs: [n]f64) =
  f64.sum vs / f64.i64 n

def var [n] (vs: [n]f64) =
  let m = mean vs
  let xs = vs |> map (\x -> (x-m) ** 2)
  in f64.sum xs / (f64.i64 n)

def main (n: f64): f64 =
  n * var [1.0,2.0,3.0,4.0,5.0]
