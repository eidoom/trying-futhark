TARGET=multicore

SRC=$(wildcard *.fut)
PRG=$(basename $(SRC))

.PHONY: all

all: $(PRG)

%: %.fut
	futhark $(TARGET) --executable --Werror -o $@ $<
