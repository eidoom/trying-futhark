def matmul [n][m][p] 'a
           (add: a-> a -> a) (mul: a-> a -> a) (zero:a)
           (A: [n][m]a) (B: [m][p]a) : [n][p]a =
  map (\A_row ->
    map (\B_col ->
      map2 mul A_row B_col |> reduce add zero)
      (transpose B))
    A

def matmul_i32 =
  matmul (+) (*) 0i32

def scamul [n][m] 't
           (mul: t -> t -> t)
           (a: t) (mat: [n][m]t) : [n][m]t =
  mat |> map (\row -> 
    row |> map (\ el -> mul a el))

def scamul_i32 =
  scamul (i32.*)

def main(n: i32): [2][2]i32 =
  scamul_i32 n (matmul_i32 [[1,2],[3,4]] [[5,6],[7,8]])
