# [trying futhark](https://gitlab.com/eidoom/trying-futhark)

[Companion post](https://computing-blog.netlify.app/post/futhark/)

Learning from [examples](https://futhark-lang.org/examples.html).

```shell
make -j
echo 19 | ./fact
```
