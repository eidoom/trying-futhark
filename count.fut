def count (cond: i64 -> bool) (xs: []i64): i64 =
  xs |> map (cond >-> i64.bool) |> i64.sum

def main(n: i64): i64 =
  count (>10) (1...n)
