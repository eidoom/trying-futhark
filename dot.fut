def v0 (n: i64) =
  1...n :> [n]i64

def v1 (n: i64) =
  n+1...2*n :> [n]i64

def dot [n] (a: [n]i64) (b: [n]i64) : i64 =
  map2 (*) a b |> i64.sum

def main(n: i64): i64 =
  dot (v0 n) (v1 n)
